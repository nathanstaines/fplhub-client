# FPL Hub client

> Here exists the FPL Hub client

## Setup

``` bash
# Install dependencies
npm install

# Run local dev server
npm start
```

## Deployment

``` bash
# Build for production
npm run build
```

Upload the `dist/` directory to Amazon S3, then manually set the following metadata for each file:

```
Cache-Control: public, max-age=31536000
```

Invalidate `/index.html` in the Amazon CloudFront distribution.

## SSL generation

Use [OpenSSL](http://docs.aws.amazon.com/IAM/latest/UserGuide/server-certs_manage_create.html#certs-install-openssl) to generate a private key:

``` bash
openssl genrsa 2048 > private-key.pem
```

Then using the private key generate a CSR:

``` bash
openssl req -new -key private-key.pem -out csr.pem
```

- **Country name:** GB
- **State of Province:** England
- **Locality name:** London
- **Common Name:** fplhub.com

Activate the SSL certificate using the instructions outlined in the [Namecheap guide](https://www.namecheap.com/support/knowledgebase/article.aspx/794/67/how-do-i-activate-an-ssl-certificate) (using the HTTP-based validation method).

Add txt file from namecheap to s3 bucket.

Upload the certificate to the [AWS Certificate Manager](https://console.aws.amazon.com/acm/home) (us-east-1).

1. The Certificate in the "Certificate body" form;
2. CA-bundle in the "Certificate chain" form;
3. And the Private key in the "Private key" form.

Update CloudFront to use the new certificate.
