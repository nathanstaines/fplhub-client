import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import fixtures from './modules/fixtures';
import loaders from './modules/loaders';
import pagination from './modules/pagination';
import players from './modules/players';
import positions from './modules/positions';
import teams from './modules/teams';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    fixtures,
    loaders,
    pagination,
    players,
    positions,
    teams
  },
  actions: {
    resetAll({commit}) {
      commit('pagination/resetPaginationState');
      commit('players/resetPlayersState');
      commit('fixtures/resetFixturesState');
    }
  }
});
