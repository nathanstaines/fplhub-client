import app from '@/feathers';
import {format, parseISO} from 'date-fns';

const initialState = () => ({
  fixtures: []
});

const state = initialState;

const getters = {
  groupedFixtures(state) {
    const obj = {};

    state.fixtures.forEach(fixture => {
      const day = fixture.eventDay;

      if (!obj[day]) {
        obj[day] = {
          title: format(parseISO(fixture.kickoffTime), 'eee do MMM y'),
          fixtures: []
        };
      }

      obj[day].fixtures.push(fixture);
    });

    return obj;
  },
  hasGroupedFixtures(state, getters) {
    return Object.keys(getters.groupedFixtures).length;
  }
};

const mutations = {
  resetFixturesState(state) {
    const _state = initialState();

    Object.keys(_state).forEach(key => {
      state[key] = _state[key];
    });
  },
  setFixtures(state, fixtures) {
    state.fixtures = fixtures.map(fixture => ({
      ...fixture,
      forceCollapse: true,
      isCollapsed: true
    }));
  }
};

const actions = {
  fetchFixtures({commit, dispatch, rootState}) {
    const timeout = setTimeout(() => {
      dispatch('loaders/startLoader', 'fixtures', {root: true});
    }, 300);

    const query = {
      $limit: 0,
      $sort: {id: -1},
      $or: [{isNext: true}, {isCurrent: true}, {hasFinished: true}]
    };

    // Perform a fast counting query first.
    return app
      .service('api/events')
      .find({query})
      .then(res => {
        query.$limit = 1;

        if (res.total > 1) {
          query.$skip =
            rootState.pagination._skip !== null
              ? rootState.pagination._skip
              : 1;
        }

        app
          .service('api/events')
          .find({query})
          .then(res => {
            dispatch(
              'pagination/setPaginationValues',
              {
                limit: res.limit,
                skip: res.skip,
                total: res.total
              },
              {
                root: true
              }
            );

            commit('setFixtures', res.data[0].fixtures);
            dispatch('loaders/stopLoader', 'fixtures', {root: true});
            clearTimeout(timeout);
          });
      });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
