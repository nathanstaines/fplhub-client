import app from '@/feathers';

const state = {
  positions: []
};

const getters = {
  orderedPositions(state) {
    return state.positions.sort((a, b) => a.id - b.id);
  }
};

const mutations = {
  setPositions(state, positions) {
    state.positions = positions;
  }
};

const actions = {
  fetchPositions({commit, dispatch}) {
    const timeout = setTimeout(() => {
      dispatch('loaders/startLoader', 'positions', {root: true});
    }, 300);

    return app
      .service('api/positions')
      .find()
      .then(res => {
        commit('setPositions', res.data);
        dispatch('loaders/stopLoader', 'positions', {root: true});
        clearTimeout(timeout);
      });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
