const initialState = () => ({
  _skip: null,
  limit: 0,
  range: 5,
  separator: '...',
  skip: 0,
  total: 0
});

const state = initialState;

const getters = {
  _skip(state) {
    return state._skip;
  },
  activePageNum(state, getters) {
    return getters.currPageNum === getters.lastPageNum
      ? getters.lastPageNum - 1
      : getters.currPageNum;
  },
  currChunkNum(state, getters) {
    return Math.floor(getters.activePageNum / state.range) * state.range;
  },
  currPageNum(state) {
    return Math.ceil(state.skip / state.limit);
  },
  lastPageNum(state) {
    return Math.ceil(state.total / state.limit) - 1;
  },
  pageItems(state, getters) {
    return Array.from(
      {
        length: getters.totalPageNum
      },
      (val, i) => ({index: i, value: i})
    );
  },
  paginationItems(state, getters) {
    const items = getters.pageItems
      .filter(item => item.index !== getters.lastPageNum)
      .slice(getters.currChunkNum, getters.currChunkNum + state.range);

    if (getters.activePageNum >= state.range) {
      items.unshift({
        index: getters.currChunkNum - 1,
        value: state.separator
      });

      items.unshift({
        index: 0,
        value: 0
      });
    }

    if (getters.lastPageNum - state.range > getters.currChunkNum) {
      items.push({
        index: getters.currChunkNum + state.range,
        value: state.separator
      });
    }

    items.push({
      index: getters.lastPageNum,
      value: getters.lastPageNum
    });

    return items;
  },
  totalPageNum(state) {
    return Math.ceil(state.total / state.limit);
  }
};

const mutations = {
  resetPaginationState(state) {
    const _state = initialState();

    Object.keys(_state).forEach(key => {
      state[key] = _state[key];
    });
  },
  setPaginationState(state, {key, val}) {
    state[key] = val;
  }
};

const actions = {
  setPaginationValues({commit}, payload) {
    Object.entries(payload).forEach(([key, val]) =>
      commit('setPaginationState', {key, val})
    );
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
