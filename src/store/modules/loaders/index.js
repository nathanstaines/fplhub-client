const uniq = array => array.filter((el, i, arr) => i === arr.indexOf(el));

const state = {
  activeLoaders: [],
  hasInitialised: false,
  isLoadingMsgEnabled: true,
  loadingMsg: '',
  loadingMsgs: [
    'The atmosphere here is thick and fast - Chris Kamara',
    "You can't win anything with kids - Alan Hansen",
    'I can see the carrot at the end of the tunnel - Stuart Pearce',
    'I never make predictions, and I never will - Paul Gascoigne',
    "It's an unprecedented precedent - Clark Carlisle"
  ]
};

const getters = {
  anyLoading(state) {
    return state.activeLoaders.length > 0;
  },
  isLoading(state) {
    return id => state.activeLoaders.indexOf(id) > -1;
  }
};

const mutations = {
  disableLoadingMsg(state) {
    state.isLoadingMsgEnabled = false;
  },
  enableLoadingMsg(state) {
    state.isLoadingMsgEnabled = true;
  },
  setInitialised(state) {
    state.hasInitialised = true;
  },
  setMessage(state) {
    const msgs = state.loadingMsgs;

    state.loadingMsg = msgs[Math.floor(Math.random() * msgs.length)];
  },
  startLoader(state, id) {
    state.activeLoaders.push(id);
    state.activeLoaders = uniq(state.activeLoaders);
  },
  stopLoader(state, id) {
    state.activeLoaders = uniq(state.activeLoaders).filter(
      loader => loader !== id
    );
  },
  unsetInitialised(state) {
    state.hasInitialised = false;
  }
};

const actions = {
  startLoader({commit, getters}, id) {
    if (!getters.anyLoading) {
      commit('setMessage');
    }

    commit('startLoader', id);
  },
  stopLoader({commit}, id) {
    commit('stopLoader', id);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
