import app from '@/feathers';

const state = {
  teams: []
};

const getters = {
  orderedTeams(state) {
    return state.teams.sort((a, b) => a.id - b.id);
  },
  teamShortName(state) {
    return id => {
      const team = state.teams.find(team => team.id === id);

      if (team) {
        return team.shortName;
      }

      return '???';
    };
  }
};

const mutations = {
  setTeams(state, teams) {
    state.teams = teams;
  }
};

const actions = {
  fetchTeams({commit, dispatch}) {
    const timeout = setTimeout(() => {
      dispatch('loaders/startLoader', 'teams', {root: true});
    }, 300);

    return app
      .service('api/teams')
      .find()
      .then(res => {
        commit('setTeams', res.data);
        dispatch('loaders/stopLoader', 'teams', {root: true});
        clearTimeout(timeout);
      });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
