import app from '@/feathers';

const state = {
  authErr: false,
  hasAttemptedAuth: false,
  user: null
};

const getters = {
  isAuthenticated(state) {
    return !!state.user;
  }
};

const mutations = {
  clearAuthErr(state) {
    state.authErr = null;
  },
  logout(state) {
    state.user = null;
  },
  setAuthAttempted(state) {
    state.hasAttemptedAuth = true;
  },
  setAuthErr(state, err) {
    state.authErr = err;
  },
  setUser(state, user) {
    state.user = user;
  }
};

const actions = {
  authenticate({commit, dispatch, state}, payload) {
    const timeout = setTimeout(() => {
      dispatch('loaders/startLoader', 'auth', {root: true});
    }, 300);

    if (state.authErr) {
      commit('clearAuthErr');
    }

    return app
      .authenticate(payload)
      .then(res => {
        if (res.user) {
          commit('setUser', res.user);
        }

        dispatch('loaders/stopLoader', 'auth', {root: true});
        clearTimeout(timeout);
        commit('setAuthAttempted');

        return res;
      })
      .catch(err => {
        dispatch('loaders/stopLoader', 'auth', {root: true});
        clearTimeout(timeout);
        commit('setAuthAttempted');

        if (payload) {
          commit('setAuthErr', err);
          return Promise.reject(err);
        }
      });
  },
  logout({commit, dispatch}) {
    const timeout = setTimeout(() => {
      dispatch('loaders/startLoader', 'logout', {root: true});
    }, 300);

    return app
      .logout()
      .then(res => {
        commit('logout');
        dispatch('loaders/stopLoader', 'logout', {root: true});
        clearTimeout(timeout);

        return res;
      })
      .catch(err => Promise.reject(err));
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
