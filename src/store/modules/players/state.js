export const initialState = () => ({
  _position: null,
  _search: null,
  _sort: null,
  _team: null,
  _timeframe: null,
  gameHistoryStats: [
    {
      id: 'round',
      title: 'GW',
      tooltip: 'Gameweek'
    },
    {
      id: 'points',
      title: 'Pts',
      tooltip: 'Points'
    },
    {
      id: 'minutes',
      title: 'M',
      tooltip: 'Minutes',
      filter: 'localeString'
    },
    {
      id: 'goalsScored',
      title: 'GS',
      tooltip: 'Goals scored'
    },
    {
      id: 'assists',
      title: 'A',
      tooltip: 'Assists'
    },
    {
      id: 'goalsConceded',
      title: 'GC',
      tooltip: 'Goals conceded'
    },
    {
      id: 'cleanSheets',
      title: 'CS',
      tooltip: 'Clean sheets'
    },
    {
      id: 'saves',
      title: 'S',
      tooltip: 'Saves'
    },
    {
      id: 'ownGoals',
      title: 'OG',
      tooltip: 'Own goals'
    },
    {
      id: 'yellowCards',
      title: 'YC',
      tooltip: 'Yellow cards'
    },
    {
      id: 'redCards',
      title: 'RC',
      tooltip: 'Red cards'
    },
    {
      id: 'bonus',
      title: 'B',
      tooltip: 'Bonus'
    },
    {
      id: 'bonusPointsSystem',
      title: 'BPS',
      tooltip: 'Bonus Points System',
      filter: 'localeString'
    },
    {
      id: 'price',
      title: '£',
      tooltip: 'Price',
      filter: 'gbp',
      isGlobalValue: true
    }
  ],
  players: [],
  position: 'all',
  primaryStats: [
    {
      id: 'goalsScored',
      title: 'Goals scored'
    },
    {
      id: 'assists',
      title: 'Assists'
    },
    {
      id: 'bonus',
      title: 'Bonus'
    },
    {
      id: 'form',
      title: 'Form',
      filter: 'fixed',
      isGlobal: true
    },
    {
      id: 'pointsPerGame',
      title: 'PPG',
      tooltip: 'Points per game',
      filter: 'fixed'
    },
    {
      id: 'xPoints',
      title: 'xPoints',
      tooltip: 'Expected points',
      filter: 'fixed',
      isGlobal: true
    },
    {
      id: 'value',
      title: 'Value',
      filter: 'fixed',
      isGlobal: true
    },
    {
      id: 'points',
      title: 'Points'
    },
    {
      id: 'price',
      title: 'Price',
      filter: 'gbp',
      isGlobal: true
    }
  ],
  search: '',
  searchKey: 'normalizedFullName',
  seasonHistoryStats: [
    {
      id: 'name',
      title: 'Season'
    },
    {
      id: 'points',
      title: 'Pts',
      tooltip: 'Points'
    },
    {
      id: 'minutes',
      title: 'M',
      tooltip: 'Minutes',
      filter: 'localeString'
    },
    {
      id: 'goalsScored',
      title: 'GS',
      tooltip: 'Goals scored'
    },
    {
      id: 'assists',
      title: 'A',
      tooltip: 'Assists'
    },
    {
      id: 'goalsConceded',
      title: 'GC',
      tooltip: 'Goals conceded'
    },
    {
      id: 'cleanSheets',
      title: 'CS',
      tooltip: 'Clean sheets'
    },
    {
      id: 'saves',
      title: 'S',
      tooltip: 'Saves'
    },
    {
      id: 'ownGoals',
      title: 'OG',
      tooltip: 'Own goals'
    },
    {
      id: 'yellowCards',
      title: 'YC',
      tooltip: 'Yellow cards'
    },
    {
      id: 'redCards',
      title: 'RC',
      tooltip: 'Red cards'
    },
    {
      id: 'bonus',
      title: 'B',
      tooltip: 'Bonus'
    },
    {
      id: 'bonusPointsSystem',
      title: 'BPS',
      tooltip: 'Bonus Points System',
      filter: 'localeString'
    }
  ],
  secondaryStats: [
    {
      id: 'goalsConceded',
      title: 'Goals conceded'
    },
    {
      id: 'cleanSheets',
      title: 'Clean sheets'
    },
    {
      id: 'saves',
      title: 'Saves'
    },
    {
      id: 'influence',
      title: 'Influence',
      filter: 'fixed',
      isGlobal: true
    },
    {
      id: 'creativity',
      title: 'Creativity',
      filter: 'fixed',
      isGlobal: true
    },
    {
      id: 'threat',
      title: 'Threat',
      filter: 'fixed',
      isGlobal: true
    },
    {
      id: 'ICTIndex',
      title: 'ICT Index',
      tooltip: 'Influence, creativity & threat',
      filter: 'fixed',
      isGlobal: true
    },
    {
      id: 'selectedPercent',
      title: 'Selected',
      filter: 'percentage',
      isGlobal: true
    }
  ],
  sort: 'points',
  sortPosition: 6,
  syncedStats: ['position', 'sort', 'team', 'timeframe'],
  team: 'all',
  timeframe: 'total',
  timeframes: [
    {
      id: 'total',
      title: 'All games'
    },
    {
      id: 'summary',
      title: 'Previous 6 games'
    },
    {
      id: 'active',
      title: 'Active gameweek'
    }
  ]
});

const state = initialState;

export default state;
