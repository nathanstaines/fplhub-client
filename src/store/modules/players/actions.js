import app from '@/feathers';

const actions = {
  fetchPlayers({commit, dispatch, getters, state}) {
    const timeout = setTimeout(() => {
      dispatch('loaders/startLoader', 'players', {root: true});
    }, 300);

    return app
      .service('api/players')
      .find({query: getters.playersQuery})
      .then(res => {
        dispatch(
          'pagination/setPaginationValues',
          {
            limit: res.limit,
            skip: res.skip,
            total: res.total
          },
          {
            root: true
          }
        );

        state.syncedStats.forEach(stat => {
          const val = state[`_${stat}`];

          if (val) {
            commit('setPlayersState', {
              key: stat,
              val
            });
          }
        });

        commit('setPlayersState', {
          key: 'search',
          val: state._search ? state._search : ''
        });

        commit('setPlayers', res.data);
        dispatch('loaders/stopLoader', 'players', {root: true});
        clearTimeout(timeout);
      });
  },
  searchPlayers({commit, dispatch}, e) {
    const key = '_search';
    const val = e.target.value;

    if (!val.length) {
      commit('unsetPlayersState', key);
    } else if (val.length >= 3) {
      commit('setPlayersState', {
        key,
        val: {
          $search: val
        }
      });
    }

    commit(
      'pagination/setPaginationState',
      {
        key: '_skip',
        val: 0
      },
      {
        root: true
      }
    );

    dispatch('fetchPlayers');
  },
  setPlayersValues({commit}, payload) {
    Object.entries(payload).forEach(([key, val]) =>
      commit('setPlayersState', {key, val})
    );
  },
  updatePlayersQuery({commit, dispatch}, payload) {
    dispatch('setPlayersValues', payload);

    commit(
      'pagination/setPaginationState',
      {
        key: '_skip',
        val: 0
      },
      {
        root: true
      }
    );

    dispatch('fetchPlayers');
  }
};

export default actions;
