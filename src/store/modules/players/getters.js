const getters = {
  combinedStats(state) {
    return [...state.primaryStats, ...state.secondaryStats];
  },
  getCombinedStatsFilter(state, getters) {
    return id => {
      const item = getters.combinedStats.find(stat => stat.id === id);

      return item.filter;
    };
  },
  getGameHistoryStatsFilter(state) {
    return id => {
      const item = state.gameHistoryStats.find(stat => stat.id === id);

      return item.filter;
    };
  },
  getSeasonHistoryStatsFilter(state) {
    return id => {
      const item = state.seasonHistoryStats.find(stat => stat.id === id);

      return item.filter;
    };
  },
  groupedPrimaryStats(state, getters) {
    return groupNum => {
      const arr = getters.orderedStats.slice(0, getters.primaryStatsLength);

      return arr
        .map((stat, i) =>
          i % groupNum === 0 ? arr.slice(i, i + groupNum) : null
        )
        .filter(stat => stat);
    };
  },
  hasPlayers(state) {
    return state.players.length;
  },
  isGlobalStat(state, getters) {
    const item = getters.combinedStats.find(stat => stat.id === state.sort);

    return item.isGlobal;
  },
  orderedCombinedStats(state, getters) {
    const arr = [...getters.combinedStats];

    return arr.sort((a, b) => a.title.localeCompare(b.title));
  },
  orderedStats(state, getters) {
    const arr = [...getters.combinedStats];

    const currSortIndex = arr.findIndex(stat => stat.id === state.sort);

    // Swap the last three primary stats around before moving the currently sorted item.
    if (currSortIndex > state.sortPosition) {
      const z = getters.primaryStatsLength - 1;
      const y = getters.primaryStatsLength - 2;
      const x = getters.primaryStatsLength - 3;

      if (currSortIndex === z) {
        [arr[y], arr[x]] = [arr[x], arr[y]];
      } else if (currSortIndex === y) {
        [arr[z], arr[x]] = [arr[x], arr[z]];
      } else {
        [arr[z], arr[y], arr[x]] = [arr[x], arr[z], arr[y]];
      }
    }

    // Move the currently sorted item.
    arr.splice(state.sortPosition, 0, arr.splice(currSortIndex, 1)[0]);

    return arr;
  },
  playersQuery(state, getters, rootState) {
    const query = {
      $sort: {}
    };

    const position = state._position ? state._position : state.position;
    const sort = state._sort ? state._sort : state.sort;
    const timeframe = state._timeframe ? state._timeframe : state.timeframe;
    const team = state._team ? state._team : state.team;

    if (position !== 'all') {
      query.position = position;
    }

    if (state._search) {
      query[state.searchKey] = state._search;
    }

    query.$skip =
      rootState.pagination._skip !== null
        ? rootState.pagination._skip
        : rootState.pagination.skip;

    query.$sort[`stats.${sort}.${timeframe}`] = -1;

    if (team !== 'all') {
      query.team = team;
    }

    // #TODO: Add ability to filter by price.
    // query['stats.price.total'] = {
    //   $lte: 100
    // };

    return query;
  },
  primaryStatsLength(state) {
    return state.primaryStats.length;
  },
  search(state) {
    return state.search;
  }
};

export default getters;
