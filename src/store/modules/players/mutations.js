import initialState from './state';

const mutations = {
  resetPlayersState(state) {
    const _state = initialState();

    Object.keys(_state).forEach(key => {
      state[key] = _state[key];
    });
  },
  setPlayers(state, players) {
    state.players = players.map(player => ({
      ...player,
      forceCollapse: true,
      isCollapsed: true
    }));
  },
  setPlayersState(state, {key, val}) {
    state[key] = val;
  },
  unsetPlayersState(state, key) {
    state[key] = null;
  }
};

export default mutations;
