import auth from '@feathersjs/authentication-client';
import feathers from '@feathersjs/feathers';
import io from 'socket.io-client';
import socketio from '@feathersjs/socketio-client';

const socket = io(process.env.VUE_APP_API_URL, {
  transportOptions: {
    polling: {
      extraHeaders: {
        'x-api-key': process.env.VUE_APP_API_KEY
      }
    }
  }
});

const app = feathers();

app.configure(
  socketio(socket, {
    timeout: 30000
  })
);

app.configure(
  auth({
    path: '/auth',
    service: 'api/users',
    storage: localStorage
  })
);

export default app;
