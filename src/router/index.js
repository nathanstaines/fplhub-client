import Footer from '@/components/Footer';
import Header from '@/components/Header';
import PlayerSearch from '@/components/PlayerSearch';
import Router from 'vue-router';
import Vue from 'vue';
import store from '@/store';

// Lazy load components.
const AdminView = () => import('@/components/AdminView');
const FixtureView = () => import('@/components/FixtureView');
const LoginView = () => import('@/components/LoginView');
const PlayerView = () => import('@/components/PlayerView');

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/admin',
      components: {
        header: Header,
        default: AdminView,
        footer: Footer
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      components: {
        header: Header,
        default: LoginView,
        footer: Footer
      }
    },
    {
      path: '/players',
      components: {
        header: Header,
        default: PlayerView,
        footer: Footer
      },
      children: [
        {
          path: '/',
          components: {
            search: PlayerSearch
          }
        }
      ]
    },
    {
      path: '/fixtures',
      components: {
        header: Header,
        default: FixtureView,
        footer: Footer
      }
    },
    {
      path: '*',
      redirect: '/players'
    }
  ],
  mode: 'history'
});

let isInitialRoute = true;

const handleRecord = (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters['auth/isAuthenticated']) {
      next();
    } else {
      next('/login');
    }
  } else {
    next();
  }
};

router.beforeEach((to, from, next) => {
  if (isInitialRoute) {
    store.dispatch('auth/authenticate').then(() => {
      isInitialRoute = false;
      handleRecord(to, from, next);
    });
  } else {
    handleRecord(to, from, next);
  }
});

export default router;
